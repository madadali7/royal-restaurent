package dao;


import model.Login;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class LoginDao {
    public static List<Login> getAllLogin(){
        List<Login> results = new ArrayList<>();
        try{
            ResultSet rs = DBService.query("SELECT * FROM `royal`.login;");
            while (rs.next()){
                results.add(new Login(Long.valueOf(rs.getString("login_id")),
                        rs.getString("login_username"),
                        rs.getString("login_password")));
            }
            DBService.con.close();
        }  catch (Exception e){
            System.out.println(e);
        }
        return results;
    }
}
