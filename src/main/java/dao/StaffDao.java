package dao;

import model.Customer;
import model.Staff;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class StaffDao {

    public static List<Staff> getAllStaff(){
        List<Staff> results = new ArrayList<>();

        try {
            ResultSet rs = DBService.query("SELECT * FROM `royal`.staff;");
            while (rs.next()){
                results.add(new Staff(Long.valueOf(rs.getString("staff_id")),
                        rs.getString("staff_fname"),
                        rs.getString("staff_lname"),
                        rs.getString("staff_gender"),
                        rs.getString("staff_phone"),
                        rs.getString("staff_address"),
                        rs.getString("staff_role")));
            }
            DBService.con.close();
        }   catch (Exception e){
            System.out.println(e);
        }
        return results;
    }
}
