package dao;

import model.Item;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class ItemDao {

    public static List<Item> getAllItem(){
        List<Item> results = new ArrayList<>();

        try {
            ResultSet rs = DBService.query("SELECT * FROM `royal`.item;");
            while (rs.next()){
                results.add(new Item(Long.valueOf(rs.getString("item_id")),
                        rs.getString("item_name"),
                        rs.getString("item_type"),
                        rs.getString("item_price")));
            }
            DBService.con.close();
        }   catch (Exception e){
            System.out.println(e);
        }
        return results;
    }
}
