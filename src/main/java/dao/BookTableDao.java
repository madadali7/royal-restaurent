package dao;

import model.BookTable;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class BookTableDao {

    public static List<BookTable> getAllBookTable(){
        List<BookTable> results = new ArrayList<>();
        try{
            ResultSet rs = DBService.query("SELECT * FROM `royal`.book_table;");
            while (rs.next()){
                results.add(new BookTable(Long.valueOf(rs.getString("book_table_id")),
                        rs.getString("table_id"),
                        rs.getString("is_available")
                 ));
            }
            DBService.con.close();
        }  catch (Exception e){
            System.out.println(e);
        }
        return results;
    }
}
