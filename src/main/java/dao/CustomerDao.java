package dao;

import model.Customer;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class CustomerDao {


    public static List<Customer> getAllCustomers(){
        List<Customer> results = new ArrayList<>();
        try{
            ResultSet rs = DBService.query("SELECT * FROM customer;");


            while (rs.next()){
                results.add(new Customer(Long.valueOf(rs.getString("customer_id")),
                        rs.getString("customer_name"),
                        rs.getString("customer_phone"),
                        rs.getString("table_id"),
                        rs.getString("bill_paid")));
            }
            DBService.con.close();
        }  catch (Exception e){
            System.out.println(e);
        }
        return results;
    }

}
