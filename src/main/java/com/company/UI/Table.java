package com.company.UI;

import javax.swing.*;
import java.awt.*;

public class Table {
    public Table(){

        String[] optionsToChoose = {"Table 7", "Table 5", "Table 3", "Table 9"};

//        JFrame f=new JFrame();
        JFrame f= new JFrame("Book Table For Customer");

        JLabel l1,l2,l3;
        JTextField t1,t2,t3;

        l1=new JLabel("Customer Name: ");
        l1.setBounds(400,150, 100,30);
        t1=new JTextField("John");
        t1.setBounds(400,200, 200,30);
        l2=new JLabel("Customer Phone");
        l2.setBounds(400,300, 100,30);
        t2=new JTextField("428-4029-0001");
        t2.setBounds(400,350, 200,30);
        l3=new JLabel("Available Table");
        l3.setBounds(750,150, 100,30);


        JComboBox<String> jComboBox = new JComboBox<>(optionsToChoose);
        jComboBox.setBounds(750, 200, 200, 30);


        f.add(t1);
        f.add(t2);
        f.add(l1);
        f.add(l2);
        f.add(l3);
//        f.add(t3);
        f.add(jComboBox);



        JButton add = new JButton("Book");
        add.setBounds(550,500,200,40);
        f.add(add);
        f.setSize(1386,730);
        f.setLayout(null);
        f.setVisible(true);
        f.getContentPane().setBackground(new Color(0,255,255));
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        add.addActionListener(e-> {
            String customerName = t1.getText();
            String customerPhone = t2.getText();
            String customerTable = jComboBox.getName();
            f.dispose();
            GetData check = new GetData("booked");
        });




        JButton back = new JButton("Back To Dashboard");
        back.setBounds(550,600,200,40);
        f.add(back);
        back.addActionListener(e -> {
            f.dispose();
            new HomeUi();
        });




    }
}
