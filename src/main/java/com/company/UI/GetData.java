package com.company.UI;

import com.company.Storage.BusinessLogic;

import javax.swing.*;
import java.time.LocalDate;

public class GetData {
    public GetData(String action) {
        JFrame frame = new JFrame("Resturent");
        JButton back = new JButton("Back");
        String data[][] = null;
        switch (action) {
            case "menu":
//                data = BusinessLogic.getallFlights();
                new Menu();
                break;
            case "order":
//                data = BusinessLogic.getallavailableFlights();
                new Order();
                break;

            case "bill":
//                data = BusinessLogic.getallDomesticFlights();
                break;
            case "table":
//                data = BusinessLogic.getallInternationalFlights();
                new Table();
                break;

            case "booked":
//                data = BusinessLogic.getallInternationalFlights();
                new Booked();
                break;
        }
        String[] column = {"Plane Name", "Flight Name", "Type", "Source", "destination", "flight date", "passenger"};

        JTable table = new JTable(data, column);
        table.setBounds(30, 40, 200, 300);
        back.setBounds(10, 200, 200, 30);
        JScrollPane sp = new JScrollPane(table);


        frame.add(back);
        frame.add(sp);

        frame.setSize(400, 400);
        frame.setVisible(true);

        back.addActionListener(e -> {
            frame.dispose();
            new HomeUi();
        });
    }

    public GetData(String src, String des) {

        JFrame frame = new JFrame("Flights");
        JButton back = new JButton("back");
        JButton addPasseneger = new JButton("Add passenger");
        addPasseneger.setBounds(50, 330, 200, 30);
        frame.add(addPasseneger);
        addPasseneger.addActionListener(e -> {
            frame.dispose();
            GetData passenger = new GetData("ADDPASSENGER");
        });

        String data[][] = data = BusinessLogic.getallflightsBy_Source_Destination(src, des);
        String[] column = {"Plane Name", "Flight Name", "Type", "Source", "destination", "flight date","passenger"};
        JTable table = new JTable(data, column);
        table.setBounds(30, 40, 200, 300);
        back.setBounds(10, 200, 200, 30);
        JScrollPane sp = new JScrollPane(table);


        frame.add(back);
        frame.add(sp);

        frame.setSize(400, 400);

        frame.setVisible(true);
        back.addActionListener(e -> {
            frame.dispose();
            new HomeUi();
        });
    }


}
