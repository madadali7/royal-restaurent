package com.company.UI;

import javax.swing.*;
import java.awt.*;

public class HomeUi {
    public HomeUi() {

        JFrame frame = new JFrame("Resturent Management System");
        JLabel heading = new JLabel("Resturent Management System");
        heading.setForeground(Color.ORANGE);
        heading.setBounds(300,100,1000,100);
        heading.setFont(new Font("Serif", Font.BOLD, 60));
        frame.add(heading);
//        }


        JButton menu = new JButton("Menu");
        JButton order = new JButton("Order");
        JButton bill = new JButton("Bill");
        JButton table = new JButton("Book Table For Customer");
        JButton exit = new JButton("Exit");

        menu.setBounds(550,250,300,40);
        order.setBounds(550,300,300,40);
        bill.setBounds(550,350,300,40);
        table.setBounds(550,400,300,40);
        exit.setBounds(550,450,300,40);

        frame.add(menu);
        frame.add(order);
        frame.add(bill);
        frame.add(table);
        frame.add(exit);

//        frame.setSize(800,1000);
        frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        frame.setUndecorated(true);
        frame.setLayout(null);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


        menu.addActionListener(e -> {
            frame.dispose();
            GetData check = new GetData("menu");
        });
        order.addActionListener(e -> {
            frame.dispose();
            GetData check = new GetData("order");
        });
        bill.addActionListener(e -> {
            frame.dispose();
            GetData check = new GetData("bill");
        });
        table.addActionListener(e -> {
            frame.dispose();
            GetData check = new GetData("table");
        });
        exit.addActionListener(e -> {
            System.exit(0);
        });

    }

}
