package com.company.UI;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class Menu {
    public Menu() {
        JFrame f = new JFrame();

        JButton cart = new JButton("Cart");
        cart.setBounds(20, 450, 200, 40);
        f.add(cart);

        JButton back = new JButton("Back");
        back.setBounds(20, 400, 200, 40);
        f.add(back);

        String data[][] = {
                {"1", "Roll", "150"},
                {"2", "Biryani", "200"},
                {"3", "Zinger", "400"},
                {"4", "Pizza", "1000"}};
        String column[] = {"Order ID", "Menu", "Price"};
        JTable jt = new JTable(data, column);
        jt.setBounds(10, 200, 200, 40);
        JScrollPane sp = new JScrollPane(jt);
        f.add(sp);

        jt.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int row = jt.rowAtPoint(e.getPoint());
                int col = 0;
                if (row>=0){
                    JFrame f=new JFrame("Order Quantity");
                    String name = JOptionPane.showInputDialog(f,"Add Quantity");

                    String value = jt.getModel().getValueAt(row,col).toString();
                    System.out.println(value);
                }
//                System.out.println(row);
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        f.setSize(1386, 730);
        f.setVisible(true);
        f.getContentPane().setBackground(new Color(0, 255, 255));
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


        back.addActionListener(e -> {
            f.dispose();
            new HomeUi();
        });
        cart.addActionListener(e -> {
            f.dispose();
            new HomeUi();
        });
    }
}
