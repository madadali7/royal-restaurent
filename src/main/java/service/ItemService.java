package service;

import dao.ItemDao;
import model.Item;

import java.util.List;

public class ItemService {
    static public List<Item> getAllItem(){
        return ItemDao.getAllItem();
    }
}
