package service;

import dao.StaffDao;
import model.Staff;

import java.util.List;

public class StaffService {
    static public List<Staff> getAllStaff(){
        return StaffDao.getAllStaff();
    }
}
