package service;

import dao.BookTableDao;
import model.BookTable;

import java.util.List;

public class BookTableService {

    static public List<BookTable> getAllBookTable(){
        return BookTableDao.getAllBookTable();
    }
}
