package service;

import dao.LoginDao;
import model.Login;

import java.util.List;

public class LoginService {

    static public List<Login> getAllLogin(){
        return LoginDao.getAllLogin();
    }
}
