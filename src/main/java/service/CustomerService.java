package service;

import dao.CustomerDao;
import model.Customer;

import java.util.List;

public class CustomerService {

    static public List<Customer> getAllCustomers(){
        return CustomerDao.getAllCustomers();
    }
}
