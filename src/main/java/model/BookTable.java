package model;

public class BookTable {
    private Long book_table_id;
    private String table_id;
    private String is_available;

    public BookTable(){}

    public BookTable(Long book_table_id, String table_id, String is_available) {
        this.book_table_id = book_table_id;
        this.table_id = table_id;
        this.is_available = is_available;
    }

    public Long getBook_table_id() {
        return book_table_id;
    }

    public void setBook_table_id(Long book_table_id) {
        this.book_table_id = book_table_id;
    }

    public String getTable_id() {
        return table_id;
    }

    public void setTable_id(String table_id) {
        this.table_id = table_id;
    }

    public String getIs_available() {
        return is_available;
    }

    public void setIs_available(String is_available) {
        this.is_available = is_available;
    }

    @Override
    public String toString() {
        return "BookTable{" +
                "book_table_id=" + book_table_id +
                ", table_id='" + table_id + '\'' +
                ", is_available='" + is_available + '\'' +
                '}';
    }
}
