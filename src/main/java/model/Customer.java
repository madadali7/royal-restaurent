package model;

public class Customer {
    private Long id;
    private String name;
    private String phone;
    private String table_id;
    private String bill_paid;

    public Customer(){}

    public Customer(Long id, String name, String phone, String table_id, String bill_paid) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.table_id = table_id;
        this.bill_paid = bill_paid;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getTable_id() {
        return table_id;
    }

    public void setTable_id(String table_id) {
        this.table_id = table_id;
    }

    public String getBill_paid() {
        return bill_paid;
    }

    public void setBill_paid(String bill_paid) {
        this.bill_paid = bill_paid;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                ", table_id='" + table_id + '\'' +
                ", bill_paid='" + bill_paid + '\'' +
                '}';
    }
}
