package model;

public class Staff {
    private Long staff_id;
    private String staff_fname;
    private String staff_lname;
    private String staff_gender;
    private String staff_phone;
    private String staff_address;
    private String staff_role;

    public Staff(){}

    public Staff(Long staff_id, String staff_fname, String staff_lname, String staff_gender, String staff_phone, String staff_address, String staff_role) {
        this.staff_id = staff_id;
        this.staff_fname = staff_fname;
        this.staff_lname = staff_lname;
        this.staff_gender = staff_gender;
        this.staff_phone = staff_phone;
        this.staff_address = staff_address;
        this.staff_role = staff_role;
    }

    public Long getStaff_id() {
        return staff_id;
    }

    public void setStaff_id(Long staff_id) {
        this.staff_id = staff_id;
    }

    public String getStaff_fname() {
        return staff_fname;
    }

    public void setStaff_fname(String staff_fname) {
        this.staff_fname = staff_fname;
    }

    public String getStaff_lname() {
        return staff_lname;
    }

    public void setStaff_lname(String staff_lname) {
        this.staff_lname = staff_lname;
    }

    public String getStaff_gender() {
        return staff_gender;
    }

    public void setStaff_gender(String staff_gender) {
        this.staff_gender = staff_gender;
    }

    public String getStaff_phone() {
        return staff_phone;
    }

    public void setStaff_phone(String staff_phone) {
        this.staff_phone = staff_phone;
    }

    public String getStaff_address() {
        return staff_address;
    }

    public void setStaff_address(String staff_address) {
        this.staff_address = staff_address;
    }

    public String getStaff_role() {
        return staff_role;
    }

    public void setStaff_role(String staff_role) {
        this.staff_role = staff_role;
    }

    @Override
    public String toString() {
        return "Staff{" +
                "staff_id=" + staff_id +
                ", staff_fname='" + staff_fname + '\'' +
                ", staff_lname='" + staff_lname + '\'' +
                ", staff_gender='" + staff_gender + '\'' +
                ", staff_phone='" + staff_phone + '\'' +
                ", staff_address='" + staff_address + '\'' +
                ", staff_role='" + staff_role + '\'' +
                '}';
    }
}
